package com.springboot.api.master.repository;

import com.springboot.api.master.model.Actor;
import org.hamcrest.core.Is;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActorRepository extends JpaRepository<Actor, Integer> {

    List<Actor> findByFirstName(String firstName);

    List<Actor> findByLastName(String lastName);

    List<Actor> findByStatusIsNull();

    List<Actor> findByStatusIsNotNull();
}