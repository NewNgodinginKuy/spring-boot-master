package com.springboot.api.master;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SpringbootApiMasterApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootApiMasterApplication.class, args);
	}

}
