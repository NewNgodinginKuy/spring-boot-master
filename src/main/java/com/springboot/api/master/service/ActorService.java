package com.springboot.api.master.service;

import com.springboot.api.master.model.Actor;
import com.springboot.api.master.repository.ActorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActorService {

    @Autowired
    private ActorRepository actorRepository;

    public List<Actor> getActorList() {
        return actorRepository.findAll();
    }

    public Actor getActorById(int id) {
        return actorRepository.findById(id).orElse(null);
    }

    public List<Actor> getActorByFirstName(String name) {
        return actorRepository.findByFirstName(name);
    }

    public List<Actor> getActorByLastName(String name) {
        return actorRepository.findByLastName(name);
    }

    public List<Actor> getByStatus() {
        return actorRepository.findByStatusIsNull();
    }

    public List<Actor> getByStatusNotNull() {
        return actorRepository.findByStatusIsNotNull();
    }

    public Actor saveActor(Actor actor) {
        return actorRepository.save(actor);
    }

    public List<Actor> saveActors(List<Actor> actor) {
        return actorRepository.saveAll(actor);
    }

    public String deleteActor(int id) {
        actorRepository.deleteById(id);
        return "Actor removed id :" + id;
    }

    public Actor updateActor(Actor actor) {
        Actor existingActor = actorRepository.findById(actor.getActorId()).orElse(null);
        existingActor.setFirstName(actor.getFirstName());
        existingActor.setLastName(actor.getLastName());
        return actorRepository.save(existingActor);
    }
}
