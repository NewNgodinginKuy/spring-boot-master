package com.springboot.api.master.util;

public class ConstantUtil {
    public static String STATUS = "STATUS";
    public static String REQUEST = "REQUEST";
    public static String RESPONSE = "RESPONSE";
    public static String INFO = "INFO";
    public static String DATA = "DATA";
    public static String TOKEN = "TOKEN";
//    public static String TYPE     = "TYPE";
//    public static String KYC      = "KYC";
//    public static String DOCUMENT = "DOCUMENT";
//    public static String USER     = "USER";
//    public static String SETTLEMENT = "SETTLEMENT";
//    public static String QUESTION = "QUESTION";
//    public static String SCORE = "SCORE";

    public static Integer STATUS_SUCCESS = 0;
    public static Integer STATUS_ERROR = 1;
    public static Integer STATUS_INCOMPLETE_DATA = 10;
    public static Integer STATUS_INVALID_FORMAT = 11;
    public static Integer STATUS_ACCESS_DENIED = 12;
    public static Integer STATUS_EXISTING_DATA = 13;
    public static Integer STATUS_DATA_NOT_FOUND = 50;
    public static Integer STATUS_ERROR_SYSTEM = 99;

//    public static String GLOBAL_PARAM_CUSTOMER_FILE_PATH = "CUSTOMER_FILE_PATH";
}
