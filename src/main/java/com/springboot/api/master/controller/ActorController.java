package com.springboot.api.master.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.api.master.model.Actor;
import com.springboot.api.master.service.ActorService;
import com.springboot.api.master.util.ConstantUtil;

@RestController
//@RequestMapping("/api/welcome")
public class ActorController extends BaseController {

    private static final Logger logger = LogManager.getLogger(ActorController.class);

    private static String driverName = "oracle.jdbc.driver.OracleDriver";
    private static String jdbc = "jdbc:oracle:thin:";
    private static String host = "@localhost:";
    private static String port = "1521/";
    private static String SID = "XE";
    private static String database = "XE";
    private static String url = jdbc + host + port + SID;
    private static String username = "hr";
    private static String password = "hr";

    @Autowired
    private ActorService actorService;

    @GetMapping("/v1/messages")
    public String welcome(){
        return "Welcome to Springboot Rest API";
    }
    
    @PostMapping("/v1/messages")
    public String welcomePost(){
        return "Welcome to Springboot Rest API";
    }
    
    @DeleteMapping("/v1/messages")
    public String welcomeDelete(){
        return "Welcome to Springboot Rest API";
    }
    @GetMapping("/actors")
    public List<Actor> findAllActor() {

        return actorService.getActorList();
    }

    @GetMapping("/actors/{id}")
    public Actor findByActorId(@PathVariable int id) {

        return actorService.getActorById(id);
    }

    @GetMapping("/actors/map/{id}")
    public Map findByActorIdMap(@PathVariable int id) {

        Map map = new HashMap();
        Actor resultId = actorService.getActorById(id);
        map.put("code", 0);
        map.put("info", "Succes");
        map.put("data", resultId);
        return map;
    }

    @GetMapping("/actors/id")
    public Actor findById(@RequestBody Actor actor) {

        return actorService.getActorById(actor.getActorId());
    }

    @GetMapping("/actors/map/id")
    public Map findByIdMap(@RequestBody Actor actor) {

        Map map = new HashMap();
        Actor resultId = actorService.getActorById(actor.getActorId());
        map.put("code", 0);
        map.put("info", "Succes");
        map.put("data", resultId);
        return map;
    }

    @GetMapping("/actors/name/{name}")
    public List<Actor> findByActorName(@PathVariable String name) {

        return actorService.getActorByFirstName(name);
    }

    @GetMapping("/actors/map/name/{name}")
    public Map findByActorNameMap(@PathVariable String name) {

        Map map = new HashMap();
        List<Actor> resultId = actorService.getActorByFirstName(name);
        map.put("code", 0);
        map.put("info", "Succes");
        map.put("data", resultId);
        return map;
    }

    @GetMapping("/actors/name")
    public List<Actor> findByName(@RequestBody Actor actor) {

        return actorService.getActorByFirstName(actor.getFirstName());
    }

    @GetMapping("/actors/map/name")
    public Map findByNameMap(@RequestBody Actor actor) {

        Map map = new HashMap();
        List<Actor> resultId = actorService.getActorByFirstName(actor.getFirstName());
        map.put("code", 0);
        map.put("info", "Succes");
        map.put("data", resultId);
        return map;
    }

    @GetMapping("/actors/names")
    public List<Actor> findListByName(@RequestBody Actor actor) {

        return actorService.getActorByLastName(actor.getLastName());
    }

    @GetMapping("/actors/map/names")
    public Map findListByNameMap(@RequestBody Actor actor) {

        Map map = new HashMap();
        List<Actor> resultId = actorService.getActorByLastName(actor.getLastName());
        map.put("code", 0);
        map.put("info", "Succes");
        map.put("data", resultId);
        return map;
    }

    @PostMapping("/addActor")
    public Actor addActor(@RequestBody Actor actor) {

        return actorService.saveActor(actor);
    }

    @PostMapping("/addActor/map")
    public Map addActorMap(@RequestBody Actor actor) {

        Map map = new HashMap();
        Actor resultId = actorService.saveActor(actor);
        map.put("code", 0);
        map.put("info", "Succes");
        map.put("data", resultId);
        return map;
    }

    @PostMapping("/addActors")
    public List<Actor> addActors(@RequestBody List<Actor> actors) {

        return actorService.saveActors(actors);
    }

    @PostMapping("/addActors/map")
    public Map addActorsMap(@RequestBody List<Actor> actors) {

        Map map = new HashMap();
        List<Actor> resultId = actorService.saveActors(actors);
        map.put("code", 0);
        map.put("info", "Succes");
        map.put("data", resultId);
        return map;
    }

    @PutMapping("/update")
    public Actor updateActor(@RequestBody Actor actor) {

        return actorService.updateActor(actor);
    }

    @PutMapping("/update/map")
    public Map updateActorMap(@RequestBody Actor actor) {

        Map map = new HashMap();
        Actor resultId = actorService.updateActor(actor);
        map.put("code", 0);
        map.put("info", "Succes");
        map.put("data", resultId);
        return map;
    }

    @DeleteMapping("/delete/{id}")
    public String deleteActor(@PathVariable int id) {

        return actorService.deleteActor(id);
    }

    @DeleteMapping("/delete/map/{id}")
    public Map deleteActorMap(@PathVariable int id) {

        Map map = new HashMap();
        String resultId = actorService.deleteActor(id);
        map.put("code", 0);
        map.put("info", "Succes");
        map.put("data", resultId);
        return map;
    }

    @DeleteMapping("/delete/id")
    public String deleteActors(@RequestBody Actor actor) {

        return actorService.deleteActor(actor.getActorId());
    }

    @DeleteMapping("/delete/map/id")
    public Map deleteActorsMap(@RequestBody Actor actor) {

        Map map = new HashMap();
        String resultId = actorService.deleteActor(actor.getActorId());
        map.put("code", 0);
        map.put("info", "Succes");
        map.put("data", resultId);
        return map;
    }

    @GetMapping("/actors/getLastName")
    public ResponseEntity<Map<String, Object>> findByLastName(@RequestBody(required = false) Actor actor) {

        logger.info("===== Start JSON =====");

        Map<String, Object> resultMap = new HashMap<>();
        try {
            List<Map<String, Object>> maps = new ArrayList<>();

            List<Actor> data = actorService.getActorByLastName(actor.getLastName());

            logger.info("List of Actor by Last Name");
            logger.info("=========================");

            for (Actor Actor : data) {
                Map<String, Object> actorMap = new HashMap<>();
                actorMap.put("firstName", Actor.getFirstName());
                logger.info("First Name :" + Actor.getFirstName());
                actorMap.put("lastName", Actor.getLastName());
                logger.info("Last Name :" + Actor.getLastName());

                logger.info("=========================");
                maps.add(actorMap);
            }

            resultMap = errorResponse(ConstantUtil.STATUS_SUCCESS, "Actor by Last Name", maps);
            logger.info("===== End JSON =====");

        } catch (Exception e) {
            resultMap = errorResponse(ConstantUtil.STATUS_ERROR_SYSTEM, "Actor by Last Name", null);
        }
        return new ResponseEntity<>(resultMap, HttpStatus.OK);
    }

//    @Scheduled(fixedDelay = 120000)
    public void insertStatus() {
        Date now = new Date();

        logger.info("===== Start JSON =====");

        logger.info("List of Actor by Status");
        logger.info("=========================");

        List<Actor> dataList = actorService.getByStatus();
        logger.info("Data List :" + dataList);

        for (Actor actorData : dataList) {

            logger.info("===== Insert Data =====");
            actorData.setLastUpdate(now);
            actorData.setStatus("Data Insert");
            actorService.saveActor(actorData);
            logger.info("List of Actor, Insert Data : Last Update"+ now+", Status :"+ "Data Insert");
            logger.info("=========================");
        }

        logger.info("===== End JSON =====");

    }
    //@Scheduled(cron = "0 42 8 * * *")
    //@Scheduled(fixedDelay = 120000)
    public void updateStatus() throws ClassNotFoundException, SQLException {
        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection(url, username, password);

        Date now = new Date();

        logger.info("===== Start JSON =====");

        logger.info("List of Actor by Status");
        logger.info("=========================");

        List<Actor> dataList = actorService.getByStatusNotNull();
        logger.info("Data List :" + dataList);

        for (Actor actorData : dataList) {

            logger.info("===== Update Data =====");
            actorData.setLastUpdate(now);
            actorData.setStatus("Data Update");
            actorService.saveActor(actorData);

            logger.info("List of Actor, Update Data : Last Update"+ now+", Status :"+ "Data Update");

            String query = "UPDATE EMPLOYEES SET HIRE_DATE=?, STATUS=? WHERE EMPLOYEE_ID=?";
            PreparedStatement statement = con.prepareStatement(query);
            statement.setDate(1,new java.sql.Date(now.getTime()));
            statement.setString(2, "Data Update");
            statement.setInt(3, actorData.getActorId());

            int data = statement.executeUpdate();

            if (data > 0) {
                logger.info("Data EMPLOYEE Success Update");
            }

            logger.info("=========================");
        }


        logger.info("===== End JSON =====");

    }
}