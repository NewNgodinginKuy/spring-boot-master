package com.springboot.api.master.controller;

import com.springboot.api.master.model.Actor;
import com.springboot.api.master.service.ActorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class ActorUIController {

    @Autowired
    private ActorService actorService;

    @RequestMapping("/")
    public String viewHomePage(Model model) {
        List<Actor> listActors = actorService.getActorList();
        model.addAttribute("listActors", listActors);

        return "index";
    }

    @RequestMapping("/new")
    public String showNewActorPage(Model model) {
        Actor actor = new Actor();
        model.addAttribute("actor", actor);

        return "new_actor";
    }

    @RequestMapping("/edit/{id}")
    public ModelAndView showEditActorPage(@PathVariable(name = "id") int id) {
        ModelAndView mav = new ModelAndView("edit_actor");
        Actor actor = actorService.getActorById(id);
        mav.addObject("actor", actor);

        return mav;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveActor(@ModelAttribute("actor") Actor actor) {
        actorService.saveActor(actor);

        return "redirect:/";
    }

    @RequestMapping("/delete/{id}")
    public String deleteActor(@PathVariable(name = "id") int id) {
        actorService.deleteActor(id);
        return "redirect:/";
    }
}
